# Created: MAY 2014
# Author: Lukas Treyer
# License: GPL2+

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import os, time, bpy
from .dxfimport.do import Do

def test():
    dirs = ["/Users/treyerl/Downloads/__DXF__/Manual-Importer-DXF-dxf_r12_testfiles/",
            "/Users/treyerl/Downloads/__DXF__/SAMPLE DXF FILES/",
            "/Users/treyerl/Library/Application Support/Blender/2.70/scripts/addons/io_import_dxf/testfiles"]
    imported_files = "/Users/treyerl/Downloads/__DXF__/imported_files/"

    object_merge_types = ["BY_LAYER", "BY_DXFTYPE", "SEPARATED"]
    block_rep = ["", "", "", "LINKED_OBJECTS", "GROUP_INSTANCES"]

    print("###########################################################################################################")
    print("############################################ DXF IMPORTER TEST ############################################")
    print("###########################################################################################################")

    for i in range(3):
        for k in range(3, 5):
            for j in (63, 0):  # range(16):
                bools = [bool(int(n)) for n in bin(j)[2:].zfill(6)]
                print("***********************************************************************************")
                print("options: block_representation: %s, merge_objects: %s, import_text: %s, import_light: %s, "
                      "export_acis: %s, merge_lines: %s, do_bbox: %s, recenter: %s" % (block_rep[k], object_merge_types[i],
                       str(bools[0]), str(bools[1]), str(bools[2]), str(bools[3]), str(bools[4]), str(bools[5])))
                print("===================================================================================")
                for d in dirs:
                    for f in os.listdir(d):
                        if not f.startswith("_") and (f.endswith("dxf") or f.endswith("DXF")):
                            start_time = time.time()
                            do = Do(d + os.path.sep + f, c=i, import_text=bools[0], import_light=bools[1],
                                    export_acis=bools[2], merge_lines=bools[3], do_bbox=bools[4], block_rep=k, recenter=bools[5])
                            do.entities(f.replace(".dxf", ""))
                            end_time = time.time()
                            run_time = end_time - start_time
                            print("read %s in %s seconds; " % (f, round(run_time, 3)))
                            del do
                            bpy.ops.wm.save_as_mainfile(filepath=imported_files + os.path.sep + f[:-3] +
                                                                 "_" + object_merge_types[i] + ".blend")
                            for scene in bpy.data.scenes:
                                bpy.context.screen.scene = scene
                                bpy.ops.object.select_all(action='SELECT')
                                bpy.ops.object.delete(use_global=False)
                            bpy.context.screen.scene = bpy.data.scenes["Scene"]
                            if "Blocks" in bpy.data.scenes:
                                bpy.data.scenes.remove(bpy.data.scenes["Blocks"])