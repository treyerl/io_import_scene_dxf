# Created: DEC 2013
# Author: Lukas Treyer
# License: GPL2+

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import bpy
import os
from bpy.props import StringProperty, BoolProperty, EnumProperty, IntProperty, FloatProperty
from .dxfimport.do import Do, Indicator
from .transverse_mercator import TransverseMercator


try:
    from pyproj import Proj, transform
    PYPROJ = True
except:
    PYPROJ = False

bl_info = {
    "name": "Import AutoCAD DXF Format (.dxf)",
    "author": "Lukas Treyer, Manfred Moitzi (support + dxfgrabber library)",
    "version": (0, 8, 4),
    "blender": (2, 7, 0),
    "location": "File > Import > AutoCAD DXF",
    "description": "Import files in the Autocad DXF format (.dxf)",
    "wiki_url": "https://bitbucket.org/treyerl/io_import_scene_dxf/overview",
    "tracker_url": "https://bitbucket.org/treyerl/io_import_scene_dxf/issues?status=new&status=open",
    "support": "OFFICIAL",
    "category": "Import-Export",
    }

merc_list = [('None', 'None', 'No Coordinate System is available / will be set'),
             ('tmerc', 'Transverse Mercator', 'Mercator Projection using a lat/lon coordinate as its geo-reference.')]

epsg_list=[('None', 'None', 'No Coordinate System is available / will be set'),
           ('user', 'User Defined', 'Define the EPSG code'),
           ('EPSG:4326', 'WGS84','World Geodetic System 84; default for lat / lon; EPSG:4326'),
           ('EPSG:3857', 'Spherical Mercator', 'Webbrowser mapping service standard (Google, OpenStreetMap, ESRI); EPSG:3857'),
           ('EPSG:27700', 'National Grid U.K', 'Ordnance Survey National Grid reference system used in Great Britain; EPSG:27700'),
           ('EPSG:2154', 'France (Lambert 93)', 'Lambert Projection for France; EPSG:2154'),
           ('EPSG:5514', 'Czech Republic & Slovakia', 'Coordinate System for Czech Republic and Slovakia; EPSG:5514'),
           ('EPSG:5243', 'LLC Germany', 'Projection for Germany; EPSG:5243'),
           ('EPSG:28992', 'Amersfoort Netherlands', 'Amersfoort / RD New -- Netherlands; EPSG:28992'),
           ('EPSG:21781', 'Swiss CH1903 / LV03', 'Switzerland and Lichtenstein; EPSG:21781'),
           ('EPSG:5880', 'Brazil Polyconic', 'Cartesian 2D; Central, South America; EPSG:5880 '),
           ('EPSG:42103', 'LCC USA', 'Lambert Conformal Conic Projection; EPSG:42103'),
           ('EPSG:3350', 'Russia: Pulkovo 1942 / CS63 zone C0', 'Russian Federation - onshore and offshore; EPSG:3350'),
           ('EPSG:22293', 'Cape / Lo33 South Africa', 'South Africa; EPSG:22293'),
           ('EPSG:27200', 'NZGD49 / New Zealand Map Grid', 'NZGD49 / New Zealand Map Grid; EPSG:27200'),
           ('EPSG:3112', 'GDA94 Australia Lambert', 'GDA94 / Geoscience Australia Lambert; EPSG:3112'),
           ('EPSG:24378', 'India zone I', 'Kalianpur 1975 / India zone I; EPSG:24378'),
           ('EPSG:2326', 'Hong Kong 1980 Grid System', 'Hong Kong 1980 Grid System; EPSG:2326'),
           ('EPSG:3414', 'SVY21 / Singapore TM', 'SVY21 / Singapore TM; EPSG:3414'),
           ]

__version__ = '.'.join([str(s) for s in bl_info['version']])

BY_LAYER = 0
BY_DXFTYPE = 1
SEPARATED = 2
LINKED_OBJECTS = 3
GROUP_INSTANCES = 4

T_Merge = True
T_ImportText = True
T_ImportLight = True
T_ExportAcis = False
T_MergeLines = True
T_OutlinerGroups = True
T_Bbox = True
T_CreateNewScene = False
T_Recenter = False
T_ThicknessBevel = False
T_but_group_by_att = True

RELEASE_TEST = False
DEBUG = False

def is_ref_scene():
    scn = bpy.context.scene
    return "latidute" in scn and "longitude" in scn

def read(filename, obj_merge=BY_LAYER, import_text=True, import_light=True, export_acis=True, merge_lines=True,
         do_bbox=True, block_rep=LINKED_OBJECTS, new_scene=None, recenter=False, projDXF=None, projSCN=None,
         thicknessWidth=True, but_group_by_att=True, dxf_unit_scale=1.0):
    # import dxf and export nurbs types to sat/sab files 
    # because that's how autocad stores nurbs types in a dxf...
    do = Do(filename, obj_merge, import_text, import_light, export_acis, merge_lines, do_bbox, block_rep, recenter,
            projDXF, projSCN, thicknessWidth, but_group_by_att, dxf_unit_scale)
    errors = do.entities(os.path.basename(filename).replace(".dxf", ""), new_scene)

    # display errors
    if len(errors) > 0:
        for error in errors:
            bpy.ops.error.message('INVOKE_DEFAULT', message=error)

    # inform the user about the sat/sab files
    if len(do.acis_files) > 0:
        bpy.ops.error.message('INVOKE_DEFAULT',
                              message="Exported %d NURBS objects to sat/sab files next to your DXF file."
                                      % len(do.acis_files))

def display_groups_in_outliner():
    outliners = (a for a in bpy.context.screen.areas if a.type == "OUTLINER")
    for outliner in outliners:
        outliner.spaces[0].display_mode = "GROUPS"

class MessageOperator(bpy.types.Operator):
    bl_idname = "error.message"
    bl_label = "Message"
    message = StringProperty()
 
    def execute(self, context):
        self.report({'INFO'}, self.message)
        return {'FINISHED'}
 
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=500, height=500)
 
    def draw(self, context):
        print(self.message)
        lines = []
        i = 0
        width = 41
        while len(self.message) > 0:
            lines.append(self.message[:width])
            self.message = self.message[width:]
        for line in lines:
            self.layout.label(line)

class IMPORT_OT_dxf(bpy.types.Operator):
    """Import from DXF file format (.dxf)"""
    bl_idname = "import_scene.dxf"
    bl_description = 'Import from DXF file format (.dxf)'
    bl_label = "Import DXF" + ' v.' + __version__
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_options = {'UNDO'}

    filepath = StringProperty(
            name="input file",
            subtype='FILE_PATH'
            )

    filename_ext = ".dxf"

    filter_glob = StringProperty(
            default="*.dxf",
            options={'HIDDEN'},
            )

    merge = BoolProperty(
            name="Merged Objects",
            description="Merge DXF entities to Blender objects",
            default=T_Merge,
            )

    merge_options = EnumProperty(name="Merge",
            description="Merge multiple DXF entities into one Blender object.",
            items=[('by_type', 'by Layer AND Dxf-Type','Merge DXF entities by type AND layer.'),
                   ('by_layer','by Layer', 'Merge DXF entities of a layer to an object.')],
            default='by_layer',
            )

    merge_lines = BoolProperty(
            name="Combine LINE entities to polygons",
            description="Checks if lines are connect on start or end and merges them to a polygon",
            default=T_MergeLines
            )

    import_text = BoolProperty(
            name="Import Text",
            description="Import DXF Text Entities MTEXT and TEXT",
            default=T_ImportText,
            )

    import_light = BoolProperty(
            name="Import Lights",
            description="Import DXF Text Entity LIGHT",
            default=T_ImportLight
            )

    export_acis = BoolProperty(
            name="Export ACIS Entities",
            description="Export Entities consisting of ACIS code to ACIS .sat/.sab files.",
            default=T_ExportAcis
            )

    outliner_groups = BoolProperty(
            name="Display Groups in Outliner(s)",
            description="Make all outliners in current screen layout show groups",
            default=T_OutlinerGroups
            )

    do_bbox = BoolProperty(
            name="Parent Blocks to Bounding Boxes",
            description="Create a bounding box for blocks with more than one object (faster without)",
            default=T_Bbox
            )

    block_options = EnumProperty(name="Blocks As",
            description="Select the representation of DXF blocks: linked objects or group instances. ",
            items=[('linked_objects', 'linked objects', 'block objects get imported as linked objects'),
                   ('group_instances', 'group instances', 'block objects get imported as group instances')],
            default='linked_objects',
            )

    create_new_scene = BoolProperty(
            name="Import DXF to new scene",
            description="Creates a new scene with the name of the imported file.",
            default=T_CreateNewScene
            )

    recenter = BoolProperty(
            name="Center Geometry",
            description="Moves geometry to the center of the scene",
            default=T_Recenter
            )

    thickness_and_width = BoolProperty(
            name="Line thickness and width",
            description="Map thickness and width of lines to Bevel objects and extrusion attribute",
            default=T_ThicknessBevel
            )

    but_group_by_att = BoolProperty(
            name="But group objects by attributes",
            description="If thickness and width are not imported and 'Merge objects' is on, the objects can be split by attributes",
            default=T_but_group_by_att
            )

    # geo referencing
    dxf_indi = EnumProperty(
            name="DXF coordinate type",
            description="indicated spherical or euclidian coordinates",
            items=[('euclidean', 'Euclidean', 'Coordinates in x/y'),
                   ('spherical', 'Spherical', 'Coordinates in lat/lon')],
            default='euclidean'
    )

    dxf_scale = StringProperty(  # string = the floating point precision is handled by python: float(dxf_scale)
            name="Unit Scale",
            description="coordinates are assumed to be in meters; deviation must be indicated here",
            default="1.0"
    )

    proj_dxf = EnumProperty(
            name="DXF SRID",
            description="Specifies the coordinate system for the DXF file. Check http://epsg.io",
            items=epsg_list,
            default='None',
            )

    epsg_dxf_user = StringProperty(name="EPSG-Code", default="EPSG")
    merc_dxf_lat = FloatProperty(name="Geo-Reference Latitude", default=0.0)
    merc_dxf_lon = FloatProperty(name="Geo-Reference Longitude", default=0.0)

    if PYPROJ:
        scene_proj_list = epsg_list.copy()
        scene_proj_list.insert(
            2, ('tmerc', 'Transverse Mercator', 'Mercator Projection using a lat/lon coordinate as its geo-reference.')
        )
        proj_scene = EnumProperty(
            name="Scn SRID",
            description="Specifies the coordinate system for the Scene. Check http://epsg.io",
            items=scene_proj_list,
            default='None',
            )
    else:
        proj_scene = EnumProperty(
            name="Scn SRID",
            description="Specifies the coordinate system for the Scene. Check http://epsg.io",
            items=merc_list,
            default='None',
            )

    epsg_scene_user = StringProperty(name="EPSG-Code", default="EPSG")
    merc_scene_lat = FloatProperty(name="Geo-Reference Latitude", default=0.0)
    merc_scene_lon = FloatProperty(name="Geo-Reference Longitude", default=0.0)

    def draw(self, context):
        # merge options
        self.layout.label("Merge Options:")
        col = self.layout.box().column()
        col.prop(self, "block_options")
        col.prop(self, "do_bbox")
        col.prop(self, 'merge')
        if self.merge:
            col.prop(self, 'merge_options')
        col.prop(self, "merge_lines")

        # general options
        self.layout.label("Line thickness and width:")
        col = self.layout.box().column()
        col.prop(self, "thickness_and_width")
        if not self.thickness_and_width and self.merge:
            col.prop(self, "but_group_by_att")

        # optional objects
        self.layout.label("Optional Objects:")
        col = self.layout.box().column()
        col.prop(self, 'import_text')
        col.prop(self, 'import_light')
        col.prop(self, 'export_acis')

        # view options
        self.layout.label("View Options:")
        box = self.layout.box()
        col = box.column()
        box.row().prop(self, "outliner_groups")
        box.row().prop(self, "create_new_scene")

        if self.dxf_indi == "spherical" or self.proj_scene == "tmerc" \
                or (not self.create_new_scene and is_ref_scene()):
            row = box.row()
            row.enabled = False
            self.recenter = False
            row.prop(self, "recenter")
        else:
            box.row().prop(self, "recenter")
        if not self.create_new_scene and self.recenter:
            if "latitude" in bpy.context.scene and "longitude" in bpy.context.scene:
                box.row().label("", icon="ERROR")
                box.row().label("Current scene has geo reference!")
                box.row().label("You should not center the import!")

        # geo referencing
        self.layout.label("Geo Referencing:")
        col = self.layout.box().column()

        if PYPROJ:
            self.draw_pyproj(col)
        else:
            self.draw_merc(col)

    def draw_merc(self, col):
        col.label("DXF File:")
        col.prop(self, "dxf_indi")
        if self.dxf_indi == "euclidean":
            col.prop(self, "dxf_scale")
        col.label("Geo Reference:")
        col.prop(self, "merc_scene_lat", text="lat")
        col.prop(self, "merc_scene_lon", text="lon")

    def draw_pyproj(self, col):
        valid_dxf_srid = True

        # DXF SCALE
        if self.proj_dxf == 'None':
            col.prop(self, "dxf_scale")
        else:
            try:
                p = Proj(init=self.proj_dxf)
                if not p.is_latlong():
                    col.prop(self, "dxf_scale")
            except Exception as e:
                if DEBUG:
                    print(type(e), e)

        # EPSG DXF
        col.prop(self, "proj_dxf")
        if self.proj_dxf == "user":
            try:
                Proj(init=self.epsg_dxf_user)
            except:
                col.alert = True
                valid_dxf_srid = False
            col.prop(self, "epsg_dxf_user")
            col.alert = False
        if self.proj_dxf == "tmerc":
            col.prop(self, "merc_dxf_lat", text="lat")
            col.prop(self, "merc_dxf_lon", text="lon")
        col.separator()

        # EPSG SCENE
        # warning if SCN EPSG is not None and DXF EPSG is None
        if self.proj_scene != 'None' and (not valid_dxf_srid or self.proj_dxf == 'None'):
            col.alert = True

        # draw the name of scenes EPSG if it has one already
        predefined = False
        if not self.create_new_scene:
            scn = bpy.context.scene
            if "SRID" in scn:
                predefined = True
                srid = str(scn['SRID'])
                if srid == "tmerc":
                    self.proj_scene = "tmerc"
                    self.merc_scene_lat = scn.get('latitude', 0)
                    self.merc_scene_lon = scn.get('longitude', 0)
                    col.label("Geo Reference for Transverse Mercator:")
                    col.label("Latitude: %f" % self.merc_scene_lat)
                    col.label("Longitude: %f" % self.merc_scene_lon)
                else:
                    self.proj_scene = "user"
                    self.epsg_scene_user = srid
                    for info in epsg_list:
                        if srid == info[0]:
                            srid = info[1]

                    try:
                        Proj(init=self.epsg_scene_user)
                        col.label("Scene SRID: " + str(srid))
                    except Exception as e:
                        if DEBUG:
                            print(type(e), e)
                        col.label("Scene SRID not valid; ignored", icon="ERROR")
            else:
                col.prop(self, "proj_scene")
        else:
            col.prop(self, "proj_scene")

        # validate user defined EPSG
        if self.proj_scene == "user" and not predefined:
            valid = True
            try:
                Proj(init=self.epsg_scene_user)
            except Exception as e:
                valid = False
                col.alert = True
            col.prop(self, "epsg_scene_user")
            if not valid:
                col.label("", icon="ERROR")
                col.label("invalid Scene SRID!")

            col.alert = False

        if self.proj_scene == "tmerc" and not predefined:
            col.prop(self, "merc_scene_lat", text="lat")
            col.prop(self, "merc_scene_lon", text="lon")

        # user info
        if self.proj_scene != 'None':
            if not valid_dxf_srid:
                col.label("DXF SRID not valid", icon="ERROR")
            if self.proj_dxf == 'None':
                col.label("", icon='ERROR')
                col.label("DXF SRID must be set, otherwise")
                if self.proj_scene == 'user':
                    code = self.epsg_scene_user
                else:
                    code = self.proj_scene
                col.label('scene SRID %s is ignored!' % code )

        # make sure scene EPSG is not None if DXF EPSG is not None
        if self.proj_scene == 'None' and self.proj_dxf != 'None':
            self.proj_scene = self.proj_dxf

    def execute(self, context):
        merge_map = {"by_layer": BY_LAYER, "by_type": BY_DXFTYPE}
        block_map = {"linked_objects": LINKED_OBJECTS, "group_instances": GROUP_INSTANCES}
        merge_options = SEPARATED
        if self.merge:
            merge_options = merge_map[self.merge_options]
        scene = bpy.context.scene
        if self.create_new_scene:
            scene = bpy.data.scenes.new(os.path.basename(self.filepath).replace(".dxf", ""))

        proj_dxf = None
        proj_scn = None
        if PYPROJ:
            if self.proj_dxf != 'None':
                if self.proj_dxf == 'user':
                    proj_dxf = Proj(init=self.epsg_dxf_user)
                else:
                    proj_dxf = Proj(init=self.proj_dxf)
            if self.proj_scene != 'None':
                if self.proj_scene == 'user':
                    proj_scn = Proj(init=self.epsg_scene_user)
                elif self.proj_scene == 'tmerc':
                    proj_scn = TransverseMercator(lat=self.merc_scene_lat, lon=self.merc_scene_lon)
                else:
                    proj_scn = Proj(init=self.proj_scene)
        else:
            proj_dxf = Indicator(self.dxf_indi)
            proj_scn = TransverseMercator(lat=self.merc_scene_lat, lon=self.merc_scene_lon)


        print("DXF SCALE: ", self.dxf_scale)

        if RELEASE_TEST:
            # for release testing
            from . import test
            test.test()
        else:
            read(self.filepath, merge_options, self.import_text, self.import_light, self.export_acis, self.merge_lines,
                 self.do_bbox, block_map[self.block_options], scene, self.recenter, proj_dxf, proj_scn,
                 self.thickness_and_width, self.but_group_by_att, float(self.dxf_scale))



        if self.outliner_groups:
            display_groups_in_outliner()


        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        wm.fileselect_add(self)
        return {'RUNNING_MODAL'}

def menu_func(self, context):
    self.layout.operator(IMPORT_OT_dxf.bl_idname, text="AutoCAD DXF")

def register():
    bpy.utils.register_class(MessageOperator)
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_import.append(menu_func)
 
def unregister():
    bpy.utils.unregister_module(__name__)
    #bpy.utils.unregister_class(MessageOperator)
    bpy.types.INFO_MT_file_import.remove(menu_func)

if __name__ == "__main__":
    register()